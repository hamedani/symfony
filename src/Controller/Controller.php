<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller.
 */
abstract class Controller extends AbstractController
{
    /**
     * @var array
     */
    private $metas = [];

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $message = 'Response successfully returned';

    /**
     * @var bool
     */
    private $hasErrors = false;


    /**
     * @return array
     */
    public function getMetas(): array
    {
        return $this->metas;
    }

    /**
     * @param array $metas
     *
     * @return Controller
     */
    public function setMetas(array $metas): self
    {
        $this->metas = $metas;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return Controller
     */
    protected function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    protected function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    protected function hasErrors(): bool
    {
        return $this->hasErrors;
    }

    /**
     * @param bool $hasErrors
     *
     * @return Controller
     */
    protected function setHasErrors(bool $hasErrors): self
    {
        $this->hasErrors = $hasErrors;

        return $this;
    }


    /**
     * @return Request
     */
    protected function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @required
     *
     * @param RequestStack $requestStack
     *
     * @return Controller
     */
    public function setRequest(RequestStack $requestStack): self
    {
        $this->request = $requestStack->getCurrentRequest();

        return $this;
    }


    /**
     * Sends serialized json response with default serialization group to client.
     *
     * @param array $data
     * @param int $statusCode
     * @param array $headers
     * @param array $context
     *
     * @return JsonResponse
     */
    protected function respond(
        $data = [],
        int $statusCode = Response::HTTP_OK,
        array $headers = [],
        array $context = ['groups' => 'Default']
    ): JsonResponse {
        return $this->json(
            [
                'succeed' => !$this->hasErrors(),
                'message' => $this->getMessage(),
                'results' => $data,
                'metas'   => $this->getMetas(),
            ],
            $statusCode,
            $headers,
            $context
        );
    }

    /**
     * Sends a response with error.
     *
     * @param string message
     * @param array $constraints
     * @param int $status
     *
     * @return JsonResponse
     */
    protected function respondWithError(
        string $message = 'Error has been detected!',
        array $constraints = [],
        int $status = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse {
        return $this->setHasErrors(true)->setMessage($message)->respond($constraints, $status);
    }

    /**
     * Responds validation error with messages.
     *
     * @param FormInterface $form
     *
     * @return JsonResponse
     */
    protected function respondValidatorFailed(FormInterface $form): JsonResponse
    {
        $errors = [];
        foreach ($form->getErrors(true, true) as $error) {
            $errors[$error->getOrigin()->getName()][] = $error->getMessage();
        }

        return $this->respondWithError(
            'Validation error has been detected!',
            $errors,
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * Sends a response that the object has been deleted, and also indicates
     * the id of the object that has been deleted.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    protected function respondEntityRemoved(int $id): JsonResponse
    {
        return $this->setMessage('Entity has removed successfully!')->respond(['id' => $id]);
    }

    /**
     * Sends an error when the query didn't have the right parameters for
     * creating an object.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondNotTheRightParameters($message = 'Wrong parameter has been detected!'): JsonResponse
    {
        return $this->respondWithError($message, [], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Sends a response invalid query (http 500) to the request.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondInvalidQuery($message = 'Invalid query has been detected!'): JsonResponse
    {
        return $this->respondWithError($message, [], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Sends an error when the query contains invalid parameters.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondInvalidParameters($message = 'Invalid parameters has been detected!'): JsonResponse
    {
        return $this->respondWithError($message, [], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Sends a response unauthorized (401) to the request.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondUnauthorized($message = 'Unauthorized action has been detected!'): JsonResponse
    {
        return $this->respondWithError($message, [], Response::HTTP_UNAUTHORIZED);
    }
}
